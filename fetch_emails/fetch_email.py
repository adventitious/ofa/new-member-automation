import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("onefreeapp-e2b38-bd22de4f4420.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

for doc in db.collection(u'bodyDoubleNetwork').stream():
    print(doc.id)